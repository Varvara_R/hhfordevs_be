const {Schema, model} = require("mongoose");

const Skill = new Schema({
    title: {
        type: String,
        required: true,
        unique: true
    },
    type: {
        type: String,
        required: true,
        unique: true
    },
    subType: String,
    lastVersion: String,
    officialSite: String,
    git: String,
    wiki: String,
    description: String,
    relatedLinks: [],
})

module.exports = model("Skill", Skill)