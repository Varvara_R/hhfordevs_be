const { Schema, model, ObjectId } = require("mongoose");

const User = new Schema({
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  name: String,
  roadmaps: [
    {
      type: ObjectId,
      ref: "Roadmap",
    },
  ],
  skills: [
    {
      type: ObjectId,
      ref: "Skill",
    },
  ],
  avatar: String,
  admin: Boolean,
});

module.exports = model("User", User);
