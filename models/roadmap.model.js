const { Schema, model } = require("mongoose");

const Roadmap = new Schema({
    title: {
        type: String,
        required: true,
        unique: true
    },
    data: {
        currentId: {
            type: String,
            default: "0",
        },
        skills: [String],
        elements: [
            {
                id: String,
                position: {
                    x: Number,
                    y: Number
                },
                data: {
                    label: String
                },
                type: {
                    type: String,
                    default: 'default'
                },
                style: Object,
                className: String,
                targetPosition: String,
                sourcePosition: String,
                isHidden: Boolean,
                source: String,
                target: String,
                label: String,
                animated: Boolean,
                arrowHeadType: String,
                targetHandle: String,
                sourceHandle: String
            }
        ]
    },
    created: {
        type: String,
        required: true
    },
    speciality: {
        type: String,
        required: true
    },
    description: String,
});

module.exports = model("Roadmap", Roadmap);