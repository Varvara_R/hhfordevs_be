const express = require("express");
const mongoose = require("mongoose");
const config = require("config");
const corsMiddleware = require("./middleware/cors.middleware");
const fileUpload = require("express-fileupload");

const authRouter = require("./routes/auth");
const skillRouter = require("./routes/skills");
const roadmapRouter = require("./routes/roadmaps");

const app = express();
const PORT = config.get('serverPort');

app.use(fileUpload({}))
app.use(corsMiddleware);
app.use(express.json());
app.use("/api/auth", authRouter);
app.use("/api/skills", skillRouter);
app.use("/api/roadmaps", roadmapRouter);

//to connect to db and start up server
const start = async () => {
    try {
        await mongoose.connect(config.get("dbUrl"))
        app.listen(PORT, () => {
            console.log("Server works on port ", PORT)
        })

    }catch (e) {
        console.log(e)
    }
};

start();
