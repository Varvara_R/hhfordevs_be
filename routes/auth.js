const Router = require("express");
const User = require("../models/user.model");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const config = require("config");
const { check, validationResult } = require("express-validator");
const router = new Router();

router.post(
  "/registration",
  [
    check("email", "Incorrect e-mail").isEmail(),
    check(
      "password",
      "Password must be longer than 4 and shorter than 12"
    ).isLength({ min: 4, max: 12 }),
  ],
  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json({ message: "Incorrect request", errors });
      }

      const { email, password, name } = req.body;
      const candidate = await User.findOne({ email });
      if (candidate) {
        return res
          .status(400)
          .json({ message: "User with this e-mail already exist" });
      }

      const hashPassword = await bcrypt.hash(password, 8);
      const user = new User({ email, password: hashPassword, name });
      await user.save();
      return res.json({
        message: "User was created",
        user: {
          id: user.id,
          email: user.email,
          name: user.name,
          roadmaps: user.roadmaps,
          skills: user.skills,
          avatar: user.avatar,
          admin: user.admin,
        },
      });
    } catch (err) {
      console.log(err);
      res.send({ message: "Server error while registration" });
    }
  }
);

router.post("/login", async (req, res) => {
  try {
    const { email, password } = req.body;
    const user = await User.findOne({ email });
    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }
    const isPassValid = bcrypt.compareSync(password, user.password);
    if (!isPassValid) {
      return res.status(404).json({ message: "Invalid password" });
    }
    const token = jwt.sign({ id: user.id }, config.get("secretKey"), {
      expiresIn: "1h",
    });
    return res.json({
      token,
      user: {
        id: user.id,
        email: user.email,
        name: user.name,
        roadmaps: user.roadmaps,
        skills: user.skills,
        avatar: user.avatar,
        admin: user.admin,
      },
    });
  } catch (err) {
    console.log(err);
    res.send({ message: "Server error while login" });
  }
});

module.exports = router;
