const express = require("express");
const router = express.Router();
const roadmaps = require("../controllers/roadmaps.controller");
const authMiddleWare = require("../middleware/cors.middleware");

/* GET all roadmaps
   CREATE new roadmap*/
router.route("/").get(roadmaps.list).post(roadmaps.create);

router
  .route("/:roadmapId")
  .get(roadmaps.read)
  .put(roadmaps.update)
  .delete(roadmaps.delete);

router.param("roadmapId", roadmaps.roadmapById);

module.exports = router;
