const express = require('express');
const router = express.Router();
const skills = require("../controllers/skills.controller");
const authMiddleWare = require("../middleware/cors.middleware");

/*GET all skills*/
router.route("/")
    .get(skills.list)
    .post(skills.create);

router.route('/:skillId')
    // .get(skills.read)
    // .put(skills.update)
    // .delete(skills.delete);

/*GET images */
router.route('/:skillImageId/image')
    .get(skills.getImage);

// router.param('skillId', skills.getById)
module.exports = router;