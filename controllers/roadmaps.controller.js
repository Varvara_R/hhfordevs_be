const Roadmap = require("../models/roadmap.model");

exports.list = async (req, res) => {
  try {
    const roadmaps = await Roadmap.find({});
    return res.json({ roadmaps, count: roadmaps.length });
  } catch (err) {
    console.log(err);
    res.send({ message: "Server error while getting all roadmaps" });
  }
};

exports.create = async (req, res, next) => {
  try {
    let roadmap = new Roadmap(req.body);
    await roadmap.save();
    const roadmaps = await Roadmap.find({});
    return res.json({ roadmaps: roadmaps, count: roadmaps.length });
  } catch (err) {
    res.send({ message: "Server problem while create Roadmap" });
  }
};

exports.read = (req, res) => {
  return res.json(req.roadmap);
};

exports.roadmapById = async (req, res, next, id) => {
  try {
    const roadmap = await Roadmap.findOne({
      _id: id,
    });
    req.roadmap = roadmap;
    next();
  } catch (err) {
    res.send({ message: "Server problem while getting roadmap by id" });
  }
};

exports.update = async (req, res, next) => {
  try {
    let roadmap = await Roadmap.findByIdAndUpdate(
      {
        _id: req.roadmap._id,
      },
      req.body,
      {
        new: true,
      }
    );

    return res.json({ roadmap });
  } catch (err) {
    res.send({ message: "Server problem while roadmap update" });
  }
};

exports.delete = async (req, res, next) => {
  try {
    const limit = +req.query.limit;
    const page = +req.query.page;
    await req.roadmap.remove();
    let roadmaps = await Roadmap.find({});
    if (limit && page) {
      let maps = roadmaps.slice((page - 1) * limit, page * limit);
      return res.json({ roadmaps: maps, count: roadmaps.length });
    }
    return res.json({ roadmaps: roadmaps, count: roadmaps.length });
  } catch (err) {
    console.log("Some problem while deleting of roadmap");
  }
};
