const Skill = require("../models/skill.model");
const path = require('path');
const fs = require('fs');
const sharp = require('sharp');

exports.list = async (req,res) => {
    try {
        const limit = +req.query.limit;
        const page = +req.query.page;
    
        const skills = await Skill.find({})
        if(limit && page) {
            const technologies = skills.slice((page - 1) * limit, (page * limit));
            return res.json({skills: technologies, count: skills.length})
        }else{
            return res.json({skills: skills, count: skills.length})
        }
    }
    catch(err){
        console.log(err);
        res.send({message: "Server problem during get skills"})
    }
};

exports.create = async (req,res,next) => {
    const limit = +req.query.limit;
    const page = +req.query.page;
    const data = JSON.parse(req.body.data)
    
    let skill = new Skill(data);
    if(!req.files || Object.keys(req.files).length === 0){
        return res.status(400).json({message: "No files were uploaded"});
    }
    await skill.save(function(err, skill) {
        if(err){
            return next(err)
        }
        const skillPic = req.files.file;
        const uploadPath = path.resolve('./uploads/skills/' + skill.title + '.png');
        skillPic.mv(uploadPath, (err) => {
            if(err){
                skill.delete();
                return res.status(500).json({message: err});
            }
        })
    })
    const skills =  await Skill.find({})
    if(limit && page) {
        const technologies = skills.slice((page - 1) * limit, (page * limit));
        return res.json({skills: technologies, count: skills.length})
    }
    return res.json({skills: skills, count: skills.length})
}

exports.getImage = async(req,res,next) => {
    const pathToImage = path.resolve('./uploads/skills/', req.params.skillImageId + ".png");
    if(fs.existsSync(pathToImage)){
        const buffer = await sharp(pathToImage).resize(320).toBuffer();
        res.contentType('image/jpeg');
        return res.send(buffer);
    }else{
        return status(400).json({message: "Skill image does not exist on this server"})
    }
}
